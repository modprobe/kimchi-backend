<?php

namespace App\Service;

use App\Entity\Company;
use App\Entity\User;
use App\Repository\CompanyRepository;
use App\Security\Voter\CompanyVoter;
use Monolog\Handler\TestHandler;
use PhpParser\Node\Stmt\Throw_;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CompanyService
{
    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(
        CompanyRepository $companyRepository,
        AuthorizationCheckerInterface $authorizationChecker
    )
    {
        $this->companyRepository = $companyRepository;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function save(Company $company)
    {
        $this->companyRepository->save($company);
    }
}