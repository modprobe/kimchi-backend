<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserRepository $repository, UserPasswordEncoderInterface $encoder)
    {
        $this->userRepository = $repository;
        $this->passwordEncoder = $encoder;
    }

    public function createUser(string $username, string $plainPassword, bool $isAdmin = false)
    {
        $newUser = new User();
        $newUser->setUsername($username);

        $newUser->setPassword(
            $this->passwordEncoder->encodePassword($newUser, $plainPassword)
        );

        if ($isAdmin) {
            $newUser->addRole('ROLE_ADMIN');
        }

        $this->userRepository->save($newUser);

        return $newUser;
    }

    /**
     * @param User $user
     */
    public function save(User $user)
    {
        $this->userRepository->save($user);
    }
}