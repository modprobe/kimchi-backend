<?php

namespace App\Repository;

use App\Entity\TimesheetEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TimesheetEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimesheetEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimesheetEntry[]    findAll()
 * @method TimesheetEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimesheetEntryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TimesheetEntry::class);
    }

//    /**
//     * @return TimesheetEntry[] Returns an array of TimesheetEntry objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TimesheetEntry
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
