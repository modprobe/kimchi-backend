<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\User;
use App\Form\Type\ProjectAddUsersType;
use App\Form\Type\ProjectType;
use App\Repository\ProjectRepository;
use App\Service\UserService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Rest\Route("/project", name="project.")
 */
class ProjectController extends Controller
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(ProjectRepository $projectRepository, UserService $userService)
    {
        $this->projectRepository = $projectRepository;
        $this->userService = $userService;
    }

    /**
     * @Rest\Get("", name="browse")
     * @Rest\View()
     *
     * @return Project[]
     */
    public function browse()
    {
        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->projectRepository->findAll();
        }

        /** @var User $user */
        $user = $this->getUser();

        return $user->getProjects();
    }

    /**
     * @Rest\Post("", name="create")
     * @Rest\View
     *
     * @param Request $request
     *
     * @return Project|FormInterface
     */
    public function create(Request $request)
    {
        $newProject = new Project();

        $form = $this->createForm(ProjectType::class, $newProject);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $this->projectRepository->save($newProject);

        return $newProject;
    }

    /**
     * @Rest\Post("/{id}/add-users", name="add_users")
     * @Rest\View(serializerGroups={"Default", "project.with_users"})
     *
     * @param Project $project
     * @param Request $request
     *
     * @return Project|FormInterface
     */
    public function addUser(Project $project, Request $request)
    {
        $form = $this->createForm(ProjectAddUsersType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        foreach ($form->getData()['users'] as $user) {
            /** @var User $user */
            $user = $user['username'];

            $project->addUser($user);

            $this->userService->save($user);
            $this->projectRepository->save($project);
        }

        return $project;
    }

    /**
     * @Rest\Delete("/{id}", name="delete")
     * @Rest\View
     *
     * @param Project $project
     *
     * @return array
     */
    public function delete(Project $project)
    {
        $this->projectRepository->remove($project);

        return [
            'success' => true,
        ];
    }
}
