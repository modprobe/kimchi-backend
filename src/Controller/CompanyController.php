<?php

namespace App\Controller;

use App\Entity\Company;
use App\Form\Type\CompanyType;
use App\Service\CompanyService;
use App\Service\UserService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Rest\Route("/company", name="company.")
 */
class CompanyController extends Controller
{
    /**
     * @var CompanyService
     */
    private $companyService;

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(CompanyService $companyService, UserService $userService)
    {
        $this->companyService = $companyService;
        $this->userService = $userService;
    }

    /**
     * @Rest\Get("", name="browse")
     * @Rest\View
     *
     * @return Company[]
     */
    public function browse()
    {
        return [];
    }

    /**
     * @Rest\Get("/{id}", name="read")
     * @Rest\View
     *
     * @param Company $company
     *
     * @return Company
     */
    public function read(Company $company)
    {
        return $company;
    }

    /**
     * @Rest\Post("", name="add")
     * @Rest\View
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     *
     * @return Company|FormInterface
     */
    public function add(Request $request)
    {
        $newCompany = new Company();

        $form = $this->createForm(CompanyType::class, $newCompany);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $this->companyService->save($newCompany);

        return $newCompany;
    }

    /**
     * @Rest\Put("/{id}", name="edit")
     * @Rest\View
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Company $company
     * @param Request $request
     *
     * @return Company|FormInterface
     */
    public function edit(Company $company, Request $request)
    {
        $form = $this->createForm(CompanyType::class, $company);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $this->companyService->save($company);

        return $company;
    }
}
