<?php


namespace App\Form\Transformer;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class UserToUsernameTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    public function __construct(ObjectManager $objectManager)
    {
        $this->om = $objectManager;
    }

    /**
     * {@inheritdoc}
     *
     * @param User $value
     */
    public function transform($value)
    {
        if (!($value instanceof User)) {
            return;
        }

        return ['username' => $value->getUsername()];
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        $user = $this->om
            ->getRepository('App:User')
            ->findOneBy([
                    'username' => $value,
            ]);

        if (is_null($user)) {
            throw new TransformationFailedException(
                sprintf('No user with username "%s" could be found.', $value)
            );
        }

        return $user;
    }
}