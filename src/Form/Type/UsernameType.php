<?php

namespace App\Form\Type;

use App\Form\Transformer\UserToUsernameTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Transforms a username into a User entity.
 *
 * Expects the following structure:
 *
 * <pre>
 * ... {
 *   "username": "foobar"
 * }
 * </pre>
 */
class UsernameType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    public function __construct(ObjectManager $objectManager)
    {
        $this->om = $objectManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userTransformer = new UserToUsernameTransformer($this->om);

        $builder
            ->add(
                $builder->create('username', TextType::class)
                    ->addModelTransformer($userTransformer)
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}
