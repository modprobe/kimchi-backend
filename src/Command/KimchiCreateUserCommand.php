<?php

namespace App\Command;

use App\Service\UserService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class KimchiCreateUserCommand extends Command
{
    protected static $defaultName = 'kimchi:create-user';

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;

        parent::__construct(self::$defaultName);
    }

    protected function configure()
    {
        $this
            ->setDescription('Create an application user')
            ->addArgument('username', InputArgument::REQUIRED, 'Username of new user')
            ->addOption('admin', null, InputOption::VALUE_NONE, 'Create an admin user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $username = $input->getArgument('username');
        $isAdmin = $input->getOption('admin');

        $io->section('Create a new user');

        $plainPassword = $io->askHidden('Password for the new user');

        $newUser = $this->userService->createUser($username, $plainPassword, $isAdmin);

        $io->success('Created a new user named \''.$newUser->getUsername().'\' ('.$newUser->getId().')');
    }
}
