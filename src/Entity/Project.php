<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @var TimesheetEntry[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TimesheetEntry", mappedBy="project")
     * @Serializer\Groups({"project.with_timesheet"})
     */
    private $timesheetEntries;

    /**
     * @var User[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="projects")
     * @Serializer\Groups({"project.with_users"})
     */
    private $users;

    public function __construct()
    {
        $this->timesheetEntries = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Project $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|TimesheetEntry[]
     */
    public function getTimesheetEntries(): Collection
    {
        return $this->timesheetEntries;
    }

    public function addTimesheetEntry(TimesheetEntry $timesheetEntry): self
    {
        if (!$this->timesheetEntries->contains($timesheetEntry)) {
            $this->timesheetEntries[] = $timesheetEntry;
            $timesheetEntry->setProject($this);
        }

        return $this;
    }

    public function removeTimesheetEntry(TimesheetEntry $timesheetEntry): self
    {
        if ($this->timesheetEntries->contains($timesheetEntry)) {
            $this->timesheetEntries->removeElement($timesheetEntry);
            // set the owning side to null (unless already changed)
            if ($timesheetEntry->getProject() === $this) {
                $timesheetEntry->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addProject($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeProject($this);
        }

        return $this;
    }
}
