<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, unique=true)
     * @Assert\NotBlank
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Serializer\Exclude
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var array
     * @ORM\Column(type="simple_array")
     */
    private $roles = ['ROLE_USER'];

    /**
     * @var TimesheetEntry[]|Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TimesheetEntry", mappedBy="owner")
     * @Serializer\Groups({"user.with_timesheet"})
     */
    private $timesheetEntries;

    /**
     * @var Project[]|Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Project", inversedBy="users")
     * @Serializer\Groups({"user.with_projects"})
     */
    private $projects;

    public function __construct()
    {
        $this->timesheetEntries = new ArrayCollection();
        $this->companies = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return User $this
     */
    public function setUsername(string $username): User
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User $this
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return User $this
     */
    public function setName(string $name): User
    {
        $this->name = $name;

        return $this;
}

    /**
     * @return array
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     *
     * @return User $this
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @param string|Role $role
     *
     * @return User
     */
    public function addRole($role): User
    {
        $this->roles[] = $role;

        return $this;
    }

    public function hasRole($role): bool
    {
        return in_array($role, $this->roles);
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return Collection|TimesheetEntry[]
     */
    public function getTimesheetEntries(): Collection
    {
        return $this->timesheetEntries;
    }

    public function addTimesheetEntry(TimesheetEntry $timesheetEntry): self
    {
        if (!$this->timesheetEntries->contains($timesheetEntry)) {
            $this->timesheetEntries[] = $timesheetEntry;
            $timesheetEntry->setOwner($this);
        }

        return $this;
    }

    public function removeTimesheetEntry(TimesheetEntry $timesheetEntry): self
    {
        if ($this->timesheetEntries->contains($timesheetEntry)) {
            $this->timesheetEntries->removeElement($timesheetEntry);
            // set the owning side to null (unless already changed)
            if ($timesheetEntry->getOwner() === $this) {
                $timesheetEntry->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
        }

        return $this;
    }
}